package org.microhealth.app;


/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 */


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@Configuration
@ComponentScan("org.microhealth.app.*")
@EnableJpaRepositories(basePackages = {"org.microhealth.app.*"})
@EntityScan("org.microhealth.app.*")
@EnableAutoConfiguration
@EnableSwagger2


public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}


