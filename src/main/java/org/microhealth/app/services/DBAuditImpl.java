package org.microhealth.app.services;

import java.util.Date;

import org.microhealth.app.domain.AuditMessage;
import org.microhealth.app.dto.AuditRequestDTO;
import org.microhealth.app.repository.AuditorRepository;
import org.microhealth.app.utils.CertificatesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the implementation for AuditSrevice using DB Auditing
 */

@Service("DBAuditImpl")
public class DBAuditImpl extends Auditor  implements  AuditorService{
	private static Logger logger = LoggerFactory.getLogger(DBAuditImpl.class);

	@Autowired
	private CertificatesManager certificatesManager;
	
	
	@Autowired
	private AuditorRepository auditorRepository;
	

	@Override
	public boolean audit(AuditRequestDTO auditDTO) {

		boolean isAuditSuccess = false;

			try {
				String encryptedSignature = this.signAudit(auditDTO);
				// The idea  was to store the message as encrypted in the DB using the public key of the AUDIT service and then have another service
			//	that can display the auditing message using the private key of the auditing service. But this required more time to implement
		/*		String encryptedMessage = certificatesManager
						.encryptText(
								auditDTO.getAuditMessage(),
								certificatesManager
										.getPrivate(CertificatesManager.AUDITSERVICE_PUBLIC_KEY));
										*/
	
				AuditMessage auditMessage = new AuditMessage();
				auditMessage.setMessage(auditDTO.getAuditMessage());
				auditMessage.setSignature(encryptedSignature);
				auditMessage.setUserId(auditDTO.getUserID());
				auditMessage.setCreationDate( new Date());
				auditorRepository.save(auditMessage);				
				isAuditSuccess = true;
			} catch (Exception e) {

				logger.error("Message can not be encrypted for auditDTO :"
						+ auditDTO);
				isAuditSuccess = false;
			}
			 

		return isAuditSuccess;
	}

}
