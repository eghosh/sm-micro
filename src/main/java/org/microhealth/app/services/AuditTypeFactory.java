package org.microhealth.app.services;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the Interface for creating Audit Type using factory pattern
 */

public interface AuditTypeFactory {
	  
	AuditorService getAuditType(String auditType);
	}