package org.microhealth.app.services;

import org.microhealth.app.dto.AuditRequestDTO;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description Interface for handling Auditor Service
 */

public interface AuditorService {
	
public boolean audit(AuditRequestDTO auditDTO);



}
