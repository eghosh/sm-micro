package org.microhealth.app.services;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.microhealth.app.dto.AuditRequestDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the implementation for AuditSrevice using File Auditing
 */

@Service("FileAuditImpl")
public class FileAuditImpl extends Auditor implements AuditorService {
	private static Logger logger = LoggerFactory.getLogger(FileAuditImpl.class);


private @Value("${file.audit.location}") String fileAuditPath;

	
	@Override
	public boolean audit(AuditRequestDTO auditDTO) {
		boolean isAuditSuccess = false;

		try {
			String encryptedSignature = this.signAudit(auditDTO);
			
			// The idea  was to store the message as encrypted in the DB using the public key of the AUDIT service and then have another service
			//	that can display the auditing message using the private key of the auditing service. But this required more time to implement
	
			/*
			 * String encryptedMessage = certificatesManager .encryptText(
			 * auditDTO.getAuditMessage(), certificatesManager
			 * .getPrivate(CertificatesManager.AUDITSERVICE_PUBLIC_KEY));
			 */

			// This can be moved to properties file in Application properties 
			File file =new File(fileAuditPath);
			FileWriter fw = new FileWriter(file,true);
			BufferedWriter writer = new BufferedWriter(fw);
			writer.write(auditDTO.getAuditMessage());
			writer.write(";");
			writer.write(encryptedSignature);
			writer.write(";");
			writer.write(auditDTO.getUserID());
			writer.write(";");
			writer.write(getCurrentStrDate());
			writer.newLine();
			writer.close();

			isAuditSuccess = true;
		} catch (Exception e) {

			logger.error("Message can not be encrypted for auditDTO :"
					+ auditDTO);
			isAuditSuccess = false;
		}

		return isAuditSuccess;
	}

	private String getCurrentStrDate() {
		LocalDateTime now = LocalDateTime.now();

		DateTimeFormatter formatter = DateTimeFormatter
				.ofPattern("yyyy-MM-dd HH:mm:ss");

		return now.format(formatter);
	}

}
