package org.microhealth.app.services;

import org.microhealth.app.utils.AuditorServiceTypesEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the implementation for Audit Type using factory pattern
 */

@Service("AuditTypeFactoryImpl")
public class AuditTypeFactoryImpl implements AuditTypeFactory {
	
	@Autowired
	private DBAuditImpl dbAuditImpl;
	
	@Autowired
	private FileAuditImpl fileAuditImpl;

	@Override
	public AuditorService getAuditType(String auditType) {
		if (auditType == null) {
			return null;
		}
		if (auditType.toUpperCase().equalsIgnoreCase(AuditorServiceTypesEnum.DadtabaseAudit.getValue())) {
			return dbAuditImpl;

		} else if (auditType.toUpperCase().equalsIgnoreCase(AuditorServiceTypesEnum.FileAudit.getValue())) {
			return fileAuditImpl;
		}

		return null;
	}



}
