package org.microhealth.app.services;

import org.microhealth.app.dto.AuditRequestDTO;
import org.microhealth.app.utils.CertificatesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the Abstract class for Audit Service
 */

@Component
public abstract class Auditor {


	@Autowired
	private CertificatesManager certificatesManager;
		
	public abstract boolean audit(AuditRequestDTO auditDTO);

	private static Logger logger = LoggerFactory.getLogger(Auditor.class);

	public String signAudit(AuditRequestDTO auditDTO) {
		// This method will verify the input message and the signature using the
		// public key of the user. The signature is signed with the
		// private key of the user. It will return the signature as a an encoded String

		String auditorSignator = null;
		try {
			auditorSignator = certificatesManager.signMessage(auditDTO
					.getAuditMessage());
		} catch (Exception e) {
			logger.error("Unable to validate the signature for the user"
					+ auditDTO.getUserID() + e.getMessage());
		}
		return auditorSignator;
	}

	public boolean verifySignature(AuditRequestDTO auditDTO) {

		// This method will verify the input message and the signature using the
		// public key of the user. The signature is signed with the
		// private key of the user
		boolean isvalidSignature = false;
		try {
			isvalidSignature = certificatesManager.verifyUserSignature(
					auditDTO.getAuditMessage(), auditDTO.getUserSignature());
			 isvalidSignature = true;
		} catch (Exception e) {
			logger.error("Unable to validate the signature for the user"
					+ auditDTO.getUserID() + e.getMessage());
		}
		return isvalidSignature;
	}

}
