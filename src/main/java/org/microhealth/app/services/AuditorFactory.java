package org.microhealth.app.services;

import org.springframework.stereotype.Service;
import org.microhealth.app.utils.AuditorServiceTypesEnum;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This class is not USED it is just to demo the factory Pattern
 */


//This class is not USED it is just to demo the factory Pattern

@Service("AuditorFactoryService")
public class AuditorFactory {

	private static final AuditorFactory instance = new AuditorFactory();

	private AuditorFactory() {
	}

	public static AuditorFactory getInstance() {
		return instance;
	}

	public AuditorService getAuditorType(String auditorType) {
		if (auditorType == null) {
			return null;
		}
		if (auditorType.equalsIgnoreCase(AuditorServiceTypesEnum.DadtabaseAudit.getValue())) {
			return createDBAuditor();

		} else if (auditorType.equalsIgnoreCase(AuditorServiceTypesEnum.FileAudit.getValue())) {
			return createFileAuditor();
		}

		return null;
	}

	public AuditorService createDBAuditor() {
		return new DBAuditImpl();
	}

	public AuditorService createFileAuditor() {
		return  new FileAuditImpl();
	}

}