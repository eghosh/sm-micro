package org.microhealth.app.services;

import org.microhealth.app.dto.AuditRequestDTO;
import org.microhealth.app.repository.AuditorRepository;
import org.microhealth.app.utils.CertificatesManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description implements the Interface for handling Auditor Service
 */


@Service("AuditServiceImpl")
public class AuditServiceImpl extends Auditor implements AuditorService {
	private static Logger logger = LoggerFactory.getLogger(AuditServiceImpl.class);

	@Autowired
	private CertificatesManager certificatesManager;
	
	
	@Autowired
	private AuditorRepository auditorRepository;

	
	@Autowired
	private AuditTypeFactory auditTypeFactory;
	
	

	@Override
	public boolean audit(AuditRequestDTO auditDTO) {
		AuditorService auditorService = auditTypeFactory.getAuditType(auditDTO.getAuditType());
		boolean isAuditSuccess = false;
		if (auditorService == null) {
			isAuditSuccess = false;			
		}
		else {
			// validate that the request is valid with valid signature
			boolean isValidSignature = this.verifySignature(auditDTO);
			if (isValidSignature) {
				
		       isAuditSuccess = auditorService.audit(auditDTO);
			}
			else {
				logger.error("invalid Signatures for auditDTO:" + auditDTO);
				isAuditSuccess = false;
			}
			
		}
		return isAuditSuccess;
	}

}
