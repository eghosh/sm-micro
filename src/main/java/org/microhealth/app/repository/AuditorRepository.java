package org.microhealth.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.microhealth.app.domain.AuditMessage;


/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the Repository class
 */

@Repository
public interface AuditorRepository extends JpaRepository <AuditMessage,Long>{

}