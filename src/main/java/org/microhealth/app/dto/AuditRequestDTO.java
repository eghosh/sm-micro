package org.microhealth.app.dto;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the DTO Request object for Audit
 */


public class AuditRequestDTO {

	private String userID;
	private String userSignature;
	private String auditMessage;
	private String auditType;
	
	public String getUserID() {
		return userID;
	}
	public void setUserID(String userID) {
		this.userID = userID;
	}
	public String getUserSignature() {
		return userSignature;
	}
	public void setUserSignature(String userSignature) {
		this.userSignature = userSignature;
	}
	public String getAuditMessage() {
		return auditMessage;
	}
	public void setAuditMessage(String auditMessage) {
		this.auditMessage = auditMessage;
	}
	public String getAuditType() {
		return auditType;
	}
	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}
	@Override
	public String toString() {
		return "AuditDTO [userID=" + userID + ", userSignature="
				+ userSignature + ", auditMessage=" + auditMessage
				+ ", auditType=" + auditType + "]";
	}
	
}
