package org.microhealth.app.dto;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 
 * @Description This is the DTO Response object for Audit
 */
public class AuditResponseDTO {

	private String description;
	private boolean isSuccess;
	private String  code;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String response) {
		this.description = response;
	}


	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	@Override
	public String toString() {
		return "AuditResponseDTO [description=" + description + ", isSuccess="
				+ isSuccess + ", code=" + code + "]";
	}


	
}
