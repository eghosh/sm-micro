package org.microhealth.app.webservices;

import org.microhealth.app.dto.AuditRequestDTO;
import org.microhealth.app.dto.AuditResponseDTO;
import org.microhealth.app.repository.AuditorRepository;
import org.microhealth.app.services.AuditServiceImpl;
import org.microhealth.app.services.AuditorFactory;
import org.microhealth.app.services.AuditorService;
import org.microhealth.app.services.DBAuditImpl;
import org.microhealth.app.utils.CertificatesManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 This is the Audit Service Implementation for the Audit Service
 */

@RestController
@RequestMapping("/microhealth/services/rest/1.0/")
class AuditWebServiceImpl implements AuditWebService {

	@Autowired
	private CertificatesManager certificatesManager;

	@Autowired
	@Qualifier("AuditServiceImpl")
	private AuditServiceImpl auditorService;

	private static Logger logger = LoggerFactory
			.getLogger(AuditWebServiceImpl.class);

	@Override
	@RequestMapping(value = "/audit", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> audit(@RequestBody AuditRequestDTO auditDTO) {

		logger.info(String.format("New RequestAuditDTO: %s",
				auditDTO.toString()));
		AuditResponseDTO response = new AuditResponseDTO();
		boolean isSuccessResponse = false;

		if (auditDTO == null || StringUtils.isEmpty(auditDTO.getAuditMessage())
				|| StringUtils.isEmpty(auditDTO.getAuditType())
				|| StringUtils.isEmpty(auditDTO.getUserID())
				|| StringUtils.isEmpty(auditDTO.getUserSignature())) {
			response.setDescription("Invalid Audit Request");
			response.setCode("101");
			response.setSuccess(false);
			logger.info(String.format("ResponseAuditDTO: %s",
					response.toString()));
			return new ResponseEntity<AuditResponseDTO>(response, HttpStatus.BAD_REQUEST);

		}

		// call service and get response object
		isSuccessResponse = auditorService.audit(auditDTO);
		if (isSuccessResponse) {
			response.setDescription("Audit Success");
			response.setCode("0");
			response.setSuccess(true);
		} else {
			response.setDescription("Failed Audit Service Call");
			response.setCode("102");
			response.setSuccess(false);
			logger.error(String.format("ResponseAuditDTO: %s",
					response.toString()));
			return new ResponseEntity<AuditResponseDTO>(response, HttpStatus.EXPECTATION_FAILED);
		}

		logger.info(String.format("ResponseAuditDTO: %s", response.toString()));
		return new ResponseEntity<AuditResponseDTO>(response, HttpStatus.OK);

	}
}