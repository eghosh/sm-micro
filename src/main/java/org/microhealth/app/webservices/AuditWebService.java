package org.microhealth.app.webservices;

import org.microhealth.app.dto.AuditRequestDTO;
import org.springframework.http.ResponseEntity;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 This is the Audit Interface for the Audit Service
 */

public interface AuditWebService {

	ResponseEntity<?> audit(AuditRequestDTO auditDTO);

}
