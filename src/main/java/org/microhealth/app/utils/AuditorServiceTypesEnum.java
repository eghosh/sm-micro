package org.microhealth.app.utils;

/**
 * Copyright (c) 03/04/18
 *
 * @author Emad Ghosheh
 * @version 1.0 This enum class for the Auditor Service
 */

public enum AuditorServiceTypesEnum {

	FileAudit("FILE"), DadtabaseAudit("DB");

	private final String value;

	private AuditorServiceTypesEnum(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public static AuditorServiceTypesEnum getByValue(int auditType) {
		switch (auditType) {
		case 1:
			return FileAudit;
		case 2:
			return DadtabaseAudit;
		}
		return null;
	}
}