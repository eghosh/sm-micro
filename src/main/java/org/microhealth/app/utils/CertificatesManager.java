package org.microhealth.app.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;




@Component
public class CertificatesManager {
	private Cipher cipher;

    private static Logger logger = LoggerFactory
 		   .getLogger(CertificatesManager.class);

    private @Value("${file.user.privateKey}") String userPrivateKey;
    private @Value("${file.user.publicKey}") String userPublicKey;
    private @Value("${file.audit.publicKey}") String auditPublicKey;
    private @Value("${file.audit.privateKey}") String auditPrivateKey;
    
		public byte[] sign(String input) throws Exception {
			Signature signature = Signature.getInstance("SHA256withRSA");
			signature.initSign(getPrivate(auditPrivateKey));
			processData(input, signature);
			return signature.sign();
		}

		private void processData(String input, Signature s) throws Exception {

			s.update(input.getBytes("UTF-8"));

		}
		
		public String signMessage(String signature) {
			logger.info("Message to be signed", signature);
			String encodedOutput = null;
			try {
				byte[] signatureBytes = sign(signature);
				encodedOutput = Base64.getEncoder().encodeToString(signatureBytes);
			} catch (Exception e) {
				logger.error("Unable to sign message", e);
			}
			return encodedOutput;
		} 

		public boolean verifyUserSignature(String input, String signatureStr)
				throws Exception {
			
			byte[] signatureBytes = Base64.getDecoder().decode(signatureStr);
			logger.info("verify input,signatureBytes" + input.trim() + signatureBytes);			
			Signature signature = Signature.getInstance("SHA256withRSA");
			signature.initVerify(getPublic(userPublicKey));
			signature.update(input.getBytes("UTF-8"));
			return signature.verify(signatureBytes);
		} 
		

	public PrivateKey getPrivate(String filename) throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePrivate(spec);
	}

	
	public PublicKey getPublic(String filename) throws Exception {
		byte[] keyBytes = Files.readAllBytes(new File(filename).toPath());
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}


	public String encryptText(String msg, PrivateKey key)
			throws NoSuchAlgorithmException, NoSuchPaddingException,
			UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException, InvalidKeyException {
		this.cipher.init(Cipher.ENCRYPT_MODE, key);
		return org.apache.commons.codec.binary.Base64.encodeBase64String(cipher.doFinal(msg.getBytes("UTF-8")));
	}

	public String decryptText(String msg, PublicKey key)
			throws InvalidKeyException, UnsupportedEncodingException,
			IllegalBlockSizeException, BadPaddingException {
		this.cipher.init(Cipher.DECRYPT_MODE, key);
		return new String(cipher.doFinal(org.apache.commons.codec.binary.Base64.decodeBase64(msg)), "UTF-8");
	}



}